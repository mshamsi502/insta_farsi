import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:insta_farsi/list_stories.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatelessWidget {

  final instaPost = new Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      new Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 10.0, 8.0, 0.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(left: 8.0),
                  height: 30.0,
                  width: 30.0,
                  decoration: new BoxDecoration(
                    shape:  BoxShape.circle,
                      image: new DecorationImage(
                          image: new NetworkImage(
                              'https://www.shareicon.net/data/2016/05/24/770139_man_512x512.png'
                          )
                      )
                  ),
                ),
                new Text(
                  'دوست تو',
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15.0),
                ),
              ],
            ),
            new IconButton(icon: new Icon(Icons.more_vert), onPressed: null)
          ],
        )
      ),
      new Row(
        children: <Widget>[
          new Expanded(
              child: Image.network(
                'https://www.jeanswest.ir/b2b/assets/img/brand/avakatan.png',
                fit: BoxFit.scaleDown,
              )
          )
        ],
      ),
      new Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 6.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Row(
              children: <Widget>[
                new IconButton(icon: Icon(FontAwesomeIcons.heart, color: Colors.black54 ), onPressed: null),
                new IconButton(icon: Icon(FontAwesomeIcons.comment, color: Colors.black), onPressed: null),
                new IconButton(icon: Icon(FontAwesomeIcons.paperPlane, color: Colors.black), onPressed: null),
              ],
            ),
            new IconButton(
                icon: Icon(FontAwesomeIcons.bookmark, color: Colors.black54), onPressed: null
            ),
          ],
        ),
      ),
      new Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: new Text(
          'محمد و 100,000 نفر دیگر این پست را لایک کرده اند',
           style: TextStyle(fontWeight: FontWeight.w700),
        ),
      ),
      new Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 2.0, 2.0, 0.0),
          child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget> [
               new Container(
                   width: 30.0,
                   height: 30.0,
                   margin: const EdgeInsets.only(left: 2.0, right: 10.0),
                   decoration: new BoxDecoration(
                       shape: BoxShape.circle,
                       image: DecorationImage(
                           fit: BoxFit.cover,
                           image: NetworkImage(
                               "http://www.hotavatars.com/wp-content/uploads/2019/01/I80W1Q0.png" //MyAvatar
                           )
                       )
                   )
               ),
              new Expanded(
                  child: new TextField(
                    decoration: new InputDecoration(
                     border : InputBorder.none,
                      hintText: 'اضافه کردن یک نظر...',
                      hintStyle: TextStyle(fontWeight: FontWeight.w400, fontSize: 12.0),
                    ),
                  ),
              ),
            ],
          ),
      ),
      new Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: new Text(
          '2 روز پیش',
          style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 11.0
          ),
        ),
      ),
    ],
  );
  
  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return new ListView.builder(
        itemCount: 5,
        itemBuilder: (contex, index) {
          return index == 0
              ? new SizedBox(
            child: new ListStories(),
            height: deviceSize.height * 0.162,
          )
              : instaPost;
        }
         );

  }


}